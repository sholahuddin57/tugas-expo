import React from 'react'
import { StyleSheet, Text, View, image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons'; 


export default class AboutScreen extends React.Component {
    render() {
        return(
            <View style={StyleSheet.container} >
                <Text style={styles.title}>Tentang Saya</Text>
                <Icon style={styles.title1} name= 'account-circle' size={65}/>
                <Text style={styles.item}>Muhammad Sholahuddin Nur Fuady</Text>
                <Text style={styles.subItem}>React Native Developer</Text>
            <View style={styles.tabBox}>
                <Text style={styles.tabItem}>Portofolio</Text>
                <View style={styles.tabBox2} />
                <Icon style={styles.subIcon} name= 'gitlab' size={45}/>
                <Icon style={styles.subIcon} name= 'github' size={45}/>

            <View style={styles.tabBox3}>
                <Text>Hubungi Saya</Text>
                <View style={styles.tabBox4} />
                <Icon style={styles.subIcon} name= 'youtube-searched-for' size={45} />
                    <Text style={styles.sosialMedia}>Sholah N.F.</Text>
                

            </View>

            </View>


            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 22,
        color: '#003366',
        justifyContent: 'center'
    },
    item: {
        fontSize: 18,
        color: '#003366',
        justifyContent: 'center'
    },
    subItem: {
        fontSize: 14,
        color: '#3EC6FF',
        justifyContent: 'center'
    },
    tabBox: {
        height: 160,
        width: 150,
        backgroundColor: '#EFEFEF',
    },
    tabBox2: {
        height: 0.5,
        width: 150,
        backgroundColor: 'black'
    },
    tabItem: {
        fontSize: 16,
        color: 'black',
    },
    subIcon: {
        backgroundColor: '#EFEFEF',
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabBox3: {
        height: 160,
        width: 150,
        backgroundColor: '#EFEFEF',  
    },
    tabBox4: {
        height: 0.5,
        width: 150,
        backgroundColor: 'black'
    }
})